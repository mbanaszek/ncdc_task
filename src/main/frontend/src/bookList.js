import * as React from "react";
import {Link} from 'react-router-dom';

class BookList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            books: []
        }
    }

    componentDidMount() {
        this.fetchBooks()
    }

    render() {
        return (
            <div>
                {
                    this.renderView()
                }
            </div>
        )
    }

    renderView() {
        console.log(this.state);
        if (this.state.books && this.state.books.length > 0) {
            return (
                <div className="container">
                    <h3 className="p-3 text-center">List of books</h3>
                    <table className="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th style={{width: "20%"}}>ISBN</th>
                            <th>Author</th>
                            <th>Title</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.books && this.state.books.map(book =>
                            <tr key={book.isbn}>
                                <td>{book.isbn}</td>
                                <td>{book.author}</td>
                                <td>{book.title}</td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            );
        } else {
            return (
                <div>
                    <h3 className="p-3 text-center">List of books</h3>
                    <h3 className="p-3 text-center">No records</h3>
                </div>
            )
        }
    }

    fetchBooks() {
        fetch("http://localhost:8080/api/book/list")
            .then(response => response.json().then(value => {
                    console.log(value);
                    this.setState({
                        books: value
                    });
                }
            ))
    };
}

export default function RenderBookList() {
    return (
        <div>
            <BookList />
            <div className="offset-md-1">
                <Link to="/newRecord">Add new record</Link>
            </div>
        </div>
    );
}