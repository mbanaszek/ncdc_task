import React, {useState, useRef} from "react";
import {useNavigate} from "react-router-dom";

const NewRecord = () => {

    const isbnInputRef = useRef(null);
    const authorInputRef = useRef(null);
    const titleInputRef = useRef(null);
    const [state, setState] = useState({
        isbn: '',
        author: '',
        title: '',
        isbnErrorMsg: '',
        authorErrorMsg: '',
        titleErrorMsg: '',
        addErrorMsg: ''
    });

    const navigate = useNavigate();

    const handleIsbnChange = (event) => {
        const val = event.target.value;
        setState((state) => ({...state, isbn: val}));
        isIsbnInputValidAndChangeStyle(val);
    };

    const handleAuthorChange = (event) => {
        const val = event.target.value;
        setState((state) => ({...state, author: val}));
        isAuthorInputValidAndChangeStyle(val);
    };

    const handleTitleChange = (event) => {
        const val = event.target.value;
        setState((state) => ({...state, title: val}));
        isTitleInputValidAndChangeStyle(val);
    };

    const handleAddButton = (event) => {
        let isbnValid = isIsbnInputValidAndChangeStyle(state.isbn);
        let authorValid = isAuthorInputValidAndChangeStyle(state.author);
        let titleValid = isTitleInputValidAndChangeStyle(state.title);
        if (!isbnValid || !authorValid || !titleValid) {
            return
        }
        fetch("http://localhost:8080/api/book", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(state)
        }).then(
            response => {
                console.log(response);
                if (response.status === 200) {
                    navigate('/')
                } else if (response.status === 400) {
                    response.json().then(value => {
                        if (value.errorCode === "001") {
                            isbnInputRef.current.setAttribute("class", "form-control is-invalid");
                            setState((state) => ({...state, isbnErrorMsg: "Book with such Isbn already exists"}));
                        }
                    })
                } else {
                    setState((state) => ({
                        ...state,
                        addErrorMsg: "Something went wrong when adding book. Please contact with administrator"
                    }));
                }
            }
        );
    };

    function isIsbnInputValidAndChangeStyle(value) {
        if (!value) {
            isbnInputRef.current.setAttribute("class", "form-control is-invalid");
            setState((state) => ({...state, isbnErrorMsg: "Isbn field is required"}));
            return false;
        } else {
            isbnInputRef.current.setAttribute("class", "form-control");
            setState((state) => ({...state, isbnErrorMsg: ''}));
            return true;
        }
    }

    function isAuthorInputValidAndChangeStyle(value) {
        const reg = /[^A]*\bA.*/;
        if (!value || reg.test(value) === false) {
            authorInputRef.current.setAttribute("class", "form-control is-invalid");
            setState((state) => ({
                ...state,
                authorErrorMsg: 'Author field is required and forename or surname should start\n' +
                    'from letter ‘A’'
            }));
            return false;
        } else {
            authorInputRef.current.setAttribute("class", "form-control");
            setState((state) => ({...state, authorErrorMsg: ''}));
            return true;
        }
    }

    function isTitleInputValidAndChangeStyle(value) {
        if (!value) {
            titleInputRef.current.setAttribute("class", "form-control is-invalid");
            setState((state) => ({...state, titleErrorMsg: "Title field is required"}));
            return false;
        } else {
            titleInputRef.current.setAttribute("class", "form-control");
            setState((state) => ({...state, titleErrorMsg: ''}));
            return true;
        }
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-8 offset-md-1">
                    <br/>
                    <h3>Add new book</h3>
                    <br/>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>ISBN: </label>
                            <input ref={isbnInputRef} className="form-control"
                                   id="isbn" type="text"
                                   autoFocus="autofocus"
                                   value={state.isbn} onChange={(evt) => handleIsbnChange(evt)}/>
                            <br/>
                            <span className="text-danger">{state.isbnErrorMsg}</span>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>Author: </label>
                            <input ref={authorInputRef} className="form-control"
                                   id="author"
                                   value={state.author}
                                   onChange={(evt) => handleAuthorChange(evt)}/>
                            <br/>
                            <span className="text-danger">{state.authorErrorMsg}</span>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>Title: </label>
                            <input ref={titleInputRef}
                                   className="form-control" id="title"
                                   value={state.title}
                                   onChange={(evt) => handleTitleChange(evt)}/>
                            <br/>
                            <span className="text-danger">{state.titleErrorMsg}</span>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-10 text-center">
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={(evt) => handleAddButton(evt)}
                            >
                                Add
                            </button>
                        </div>
                        <div>
                            <br/>
                            <span className="text-danger">{state.addErrorMsg}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default function RenderNewRecord() {
    return (
        <div>
            <NewRecord/>
        </div>
    );
}
