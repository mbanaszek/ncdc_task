import * as React from "react";
import * as ReactDOM from "react-dom";
import RenderBookList from "./bookList";
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import RenderNewRecord from "./newRecord";

class App extends React.Component {

    render() {
        return (
            <div>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<RenderBookList />}/>
                        <Route path="/newRecord" element={<RenderNewRecord />}/>
                    </Routes>
                </BrowserRouter>
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById("root"));