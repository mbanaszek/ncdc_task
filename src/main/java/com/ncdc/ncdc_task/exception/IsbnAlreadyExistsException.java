package com.ncdc.ncdc_task.exception;

public class IsbnAlreadyExistsException extends RuntimeException {

    public IsbnAlreadyExistsException(String isbn) {
        super("Book with ISBN [" + isbn + "] already exists");
    }
}
