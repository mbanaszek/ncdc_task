package com.ncdc.ncdc_task.repository;

import com.ncdc.ncdc_task.domain.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface BookRepository extends JpaRepository<BookEntity, UUID> {

    boolean existsByIsbn(String isbn);

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update BookEntity be set be.title = :title where be.id = :uuid")
    void updateTitle(@Param("uuid") UUID uuid, @Param("title") String title);
}
