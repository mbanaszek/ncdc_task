package com.ncdc.ncdc_task.repository;

import com.ncdc.ncdc_task.domain.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface AuthorRepository extends JpaRepository<AuthorEntity, UUID> {

    Optional<AuthorEntity> findByFullNameIgnoreCase(String fullName);

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update AuthorEntity ae set ae.fullName = :fullName where ae.id = :uuid")
    void updateFullName(@Param("uuid") UUID uuid, @Param("fullName") String fullName);
}
