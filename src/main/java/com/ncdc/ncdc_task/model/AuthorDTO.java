package com.ncdc.ncdc_task.model;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.util.UUID;

@Value
@Builder
public class AuthorDTO implements Serializable {

    UUID uuid;
    String fullName;
}
