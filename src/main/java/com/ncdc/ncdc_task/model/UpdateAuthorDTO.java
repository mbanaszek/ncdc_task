package com.ncdc.ncdc_task.model;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Value
@ToString
@Builder
public class UpdateAuthorDTO implements Serializable {

    @NotNull
    UUID uuid;

    @NotBlank
    String fullName;
}
