package com.ncdc.ncdc_task.model;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Value
@ToString
@Builder
public class CreateBookDTO implements Serializable {

    @NotBlank
    String isbn;

    @NotBlank
    @Pattern(regexp = "[^A]*\\bA.*")
    String author;

    @NotBlank
    String title;
}
