package com.ncdc.ncdc_task.model;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

@Value
@Builder
public class BookDTO implements Serializable {

    String isbn;
    String author;
    String title;
}
