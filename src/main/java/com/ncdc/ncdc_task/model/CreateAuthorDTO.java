package com.ncdc.ncdc_task.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class CreateAuthorDTO implements Serializable {

    @NotBlank
    private String fullName;
}
