package com.ncdc.ncdc_task.controller;

import com.ncdc.ncdc_task.model.BookDTO;
import com.ncdc.ncdc_task.model.CreateBookDTO;
import com.ncdc.ncdc_task.model.UpdateBookDTO;
import com.ncdc.ncdc_task.service.BookApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/book")
public class BookController {

    private final BookApplicationService bookApplicationService;

    @CrossOrigin
    @GetMapping("/list")
    List<BookDTO> getAllBooks() {
        // in frontend books are shown in simple table without paging.
        // In more complex applications paging should be done also on frontend side
        return bookApplicationService.getAllBooks(Pageable.unpaged());
    }

    @CrossOrigin
    @PostMapping
    void addBook(@RequestBody @Valid CreateBookDTO createBookDTO) {
        bookApplicationService.addBook(createBookDTO);
    }

    @CrossOrigin
    @PutMapping
    void updateBook(@RequestBody @Valid UpdateBookDTO updateBookDTO) {
        bookApplicationService.updateBook(updateBookDTO);
    }

    @CrossOrigin
    @DeleteMapping
    void deleteBook(@RequestParam(name = "uuid") UUID uuid) {
        bookApplicationService.deleteBook(uuid);
    }
}
