package com.ncdc.ncdc_task.controller;

import com.ncdc.ncdc_task.model.AuthorDTO;
import com.ncdc.ncdc_task.model.CreateAuthorDTO;
import com.ncdc.ncdc_task.model.UpdateAuthorDTO;
import com.ncdc.ncdc_task.service.AuthorApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/author")
public class AuthorController {

    private final AuthorApplicationService authorApplicationService;

    @CrossOrigin
    @GetMapping("/list")
    List<AuthorDTO> getAllAuthors(Pageable pageable) {
        return authorApplicationService.getAllAuthors(pageable);
    }

    @CrossOrigin
    @PostMapping
    void addAuthor(@RequestBody @Valid CreateAuthorDTO createAuthorDTO) {
        authorApplicationService.addAuthor(createAuthorDTO);
    }

    @CrossOrigin
    @PutMapping
    void updateAuthor(@RequestBody @Valid UpdateAuthorDTO updateAuthorDTO) {
        authorApplicationService.updateAuthor(updateAuthorDTO);
    }

    @CrossOrigin
    @DeleteMapping
    void deleteAuthor(@RequestParam(name = "uuid") UUID uuid) {
        authorApplicationService.deleteAuthor(uuid);
    }
}
