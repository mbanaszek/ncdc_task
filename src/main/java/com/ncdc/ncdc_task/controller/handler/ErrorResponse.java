package com.ncdc.ncdc_task.controller.handler;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ErrorResponse {

    String errorCode;
}
