package com.ncdc.ncdc_task.service;

import com.ncdc.ncdc_task.domain.BookEntity;
import com.ncdc.ncdc_task.model.CreateBookDTO;
import com.ncdc.ncdc_task.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorService authorService;

    public Page<BookEntity> findPageable(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    public boolean existsByIsbn(String isbn) {
        return bookRepository.existsByIsbn(isbn);
    }

    public BookEntity create(CreateBookDTO createBookDTO) {
       return bookRepository.save(prepareBookEntity(createBookDTO));
    }

    private BookEntity prepareBookEntity(CreateBookDTO createBookDTO) {
        return BookEntity.builder()
                .isbn(createBookDTO.getIsbn())
                .author(authorService.getAuthor(createBookDTO.getAuthor()))
                .title(createBookDTO.getTitle())
                .build();
    }

    public void update(UUID uuid, String newTitle) {
        bookRepository.updateTitle(uuid, newTitle);
        log.info("Modified book with id [{}]", uuid);
    }

    public void delete(UUID uuid) {
        bookRepository.deleteById(uuid);
        log.debug("Deleted book with id [{}]", uuid);
    }
}
