package com.ncdc.ncdc_task.service;

import com.ncdc.ncdc_task.domain.AuthorEntity;
import com.ncdc.ncdc_task.model.AuthorDTO;
import com.ncdc.ncdc_task.model.CreateAuthorDTO;
import com.ncdc.ncdc_task.model.UpdateAuthorDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorApplicationService {

    private final AuthorService authorService;

    public List<AuthorDTO> getAllAuthors(Pageable pageable) {
        return authorService.findPageable(pageable).stream()
                .map(this::prepareAuthorDTO)
                .collect(Collectors.toList());
    }

    private AuthorDTO prepareAuthorDTO(AuthorEntity authorEntity) {
        return AuthorDTO.builder()
                .uuid(authorEntity.getId())
                .fullName(authorEntity.getFullName())
                .build();
    }

    @Transactional
    public void addAuthor(CreateAuthorDTO createAuthorDTO) {
        AuthorEntity author = authorService.createNewAuthor(createAuthorDTO.getFullName());
        log.debug("Created author with id [{}]", author.getId());
    }

    @Transactional
    public void updateAuthor(UpdateAuthorDTO updateAuthorDTO) {
        authorService.update(updateAuthorDTO.getUuid(), updateAuthorDTO.getFullName());
    }

    @Transactional
    public void deleteAuthor(UUID uuid) {
        authorService.delete(uuid);
    }
}
