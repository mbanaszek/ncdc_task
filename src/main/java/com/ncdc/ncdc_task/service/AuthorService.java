package com.ncdc.ncdc_task.service;

import com.ncdc.ncdc_task.domain.AuthorEntity;
import com.ncdc.ncdc_task.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public Page<AuthorEntity> findPageable(Pageable pageable) {
        return authorRepository.findAll(pageable);
    }

    @Transactional
    public AuthorEntity getAuthor(String fullName) {
        // finding author should be done by id and then creating new author would have separate form in frontend,
        // but task requirement was to make one form for creating book with author full name field
        return authorRepository.findByFullNameIgnoreCase(fullName)
                .orElseGet(() -> createNewAuthor(fullName));
    }

    public AuthorEntity createNewAuthor(String fullName) {
        log.info("Created new author with full name [{}]", fullName);
        return authorRepository.save(prepareAuthor(fullName));
    }

    private AuthorEntity prepareAuthor(String fullName) {
        return AuthorEntity.builder().fullName(fullName).build();
    }

    public void update(UUID uuid, String newFullName) {
        authorRepository.updateFullName(uuid, newFullName);
        log.info("Modified author with id [{}]", uuid);
    }

    public void delete(UUID uuid) {
        authorRepository.deleteById(uuid);
        log.debug("Deleted author with id [{}]", uuid);
    }
}
