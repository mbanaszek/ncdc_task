package com.ncdc.ncdc_task.service;

import com.ncdc.ncdc_task.domain.BookEntity;
import com.ncdc.ncdc_task.exception.IsbnAlreadyExistsException;
import com.ncdc.ncdc_task.model.BookDTO;
import com.ncdc.ncdc_task.model.CreateBookDTO;
import com.ncdc.ncdc_task.model.UpdateBookDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookApplicationService {

    private final BookService bookService;

    public List<BookDTO> getAllBooks(Pageable pageable) {
        return bookService.findPageable(pageable).stream()
                .map(this::prepareBookDTO)
                .collect(Collectors.toList());
    }

    private BookDTO prepareBookDTO(BookEntity bookEntity) {
        return BookDTO.builder()
                .isbn(bookEntity.getIsbn())
                .author(bookEntity.getAuthor().getFullName())
                .title(bookEntity.getTitle())
                .build();
    }

    @Transactional
    public void addBook(CreateBookDTO createBookDTO) {
        log.info("Received create book request {}", createBookDTO);
        if (bookService.existsByIsbn(createBookDTO.getIsbn())) {
            log.warn("Book with ISBN [{}] already exists", createBookDTO.getIsbn());
            throw new IsbnAlreadyExistsException(createBookDTO.getIsbn());
        }
        BookEntity book = bookService.create(createBookDTO);
        log.debug("Created book with ISBN [{}]", book.getIsbn());
    }

    @Transactional
    public void updateBook(UpdateBookDTO updateBookDTO) {
        bookService.update(updateBookDTO.getUuid(), updateBookDTO.getTitle());
    }

    @Transactional
    public void deleteBook(UUID uuid) {
        bookService.delete(uuid);
    }
}
