package com.ncdc.ncdc_task.unit

import com.ncdc.ncdc_task.domain.BookEntity
import com.ncdc.ncdc_task.model.CreateBookDTO
import com.ncdc.ncdc_task.repository.BookRepository
import com.ncdc.ncdc_task.service.BookApplicationService
import spock.lang.Specification

class BookApplicationServiceTest extends Specification {

    BookRepository bookRepository = Mock()

    BookApplicationService bookApplicationService = new BookApplicationService(bookRepository)

    def "Should correctly map BookEntity to BookDTO"() {
        given:
        bookRepository.findAll() >> [BookEntity.builder().isbn("isbn1").author("author1").title("title1").build()]

        when:
        def books = bookApplicationService.getAllBooks()

        then:
        books.size() == 1
        with(books[0]) {
            it.isbn == "isbn1"
            it.author == "author1"
            it.title == "title1"
        }
    }

    def "Should call repository method with correct arguments"() {
        given:
        def createBookDTO = CreateBookDTO.builder().isbn("isbn2").author("author2").title("title2").build()

        when:
        bookApplicationService.addBook(createBookDTO)

        then:
        1 * bookRepository.existsByIsbn("isbn2") >> false
        1 * bookRepository.save({
            it.isbn == "isbn2"
            it.author == "author2"
            it.title == "title2"
        }) >> BookEntity.builder().build()
    }
}
