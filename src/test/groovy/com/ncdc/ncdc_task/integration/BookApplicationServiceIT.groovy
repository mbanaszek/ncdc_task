package com.ncdc.ncdc_task.integration

import com.ncdc.ncdc_task.NcdcTaskApplication
import com.ncdc.ncdc_task.domain.BookEntity
import com.ncdc.ncdc_task.exception.IsbnAlreadyExistsException
import com.ncdc.ncdc_task.model.CreateBookDTO
import com.ncdc.ncdc_task.repository.BookRepository
import com.ncdc.ncdc_task.service.BookApplicationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = NcdcTaskApplication.class)
class BookApplicationServiceIT extends Specification {

    @Autowired
    BookApplicationService bookApplicationService

    @Autowired
    BookRepository bookRepository

    def cleanup() {
        bookRepository.deleteAll()
    }

    def "Should save book entity in database"() {
        when:
        bookApplicationService.addBook(CreateBookDTO.builder().isbn("isbn3").author("author3").title("title3").build())

        then:
        def allBooks = bookRepository.findAll()
        allBooks.size() == 1
        with(allBooks[0]) {
            it.id != null
            it.isbn == "isbn3"
            it.author == "author3"
            it.title == "title3"
        }
    }

    def "Should throw IsbnAlreadyExistsException when adding book with the same isbn"() {
        given:
        bookRepository.save(BookEntity.builder().isbn("isbn4").author("author4").title("title4").build())

        when:
        bookApplicationService.addBook(CreateBookDTO.builder().isbn("isbn4").author("author5").title("title5").build())

        then:
        def e = thrown(IsbnAlreadyExistsException)
        e.message == "Book with ISBN [isbn4] already exists"
    }

}
